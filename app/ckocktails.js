const express = require('express');

const auth = require("../middleware/auth");
const Cocktail = require("../models/Cocktail");
const permit = require("../middleware/permit");
const upload = require('../multer').cocktails;

const router = express.Router();

router.post('/', auth, upload.single('image'), async (req, res) => {
    try {
        const cocktailData = {
            name: req.body.name,
            recipe: req.body.recipe,
            ingredients: JSON.parse(req.body.ingredients),
            user: req['user'],
        }

        if (req.file) {
            cocktailData.image = req.file.filename;
        }

        const cocktail = new Cocktail(cocktailData);
        const cocktailResponse = await cocktail.save(cocktail);
        return res.send(cocktailResponse);

    } catch (err) {
        return res.status(400).send(err);
    }
});

router.get('/', auth, async (req, res) => {
    try {
        const cocktailResponse = await Cocktail.find({published: true});
        return res.send(cocktailResponse);

    } catch (err) {
        return res.status(400).send(err);
    }
});

router.get('/all', auth, permit('admin'), async (req, res) => {
    try {
        const cocktailResponse = await Cocktail.find();
        return res.send(cocktailResponse);

    } catch (err) {
        return res.status(400).send(err);
    }
});

router.get('/my', auth, permit('user'), async (req, res) => {
    try {
        const cocktailResponse = await Cocktail.find({user : req['user']});
        return res.send(cocktailResponse);

    } catch (err) {
        return res.status(400).send(err);
    }
});

router.get('/:id', auth, async (req, res) => {
    try {
        const cocktailResponse = await Cocktail.findOne({_id: req.params.id});
        return res.send(cocktailResponse);

    } catch (err) {
        return res.status(400).send(err);
    }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
    try {
        const cocktailResponse = await Cocktail.findOneAndDelete({_id: req.params.id});
        return res.send(cocktailResponse);

    } catch (err) {
        return res.status(400).send(err);
    }
});

module.exports = router;